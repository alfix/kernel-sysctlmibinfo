# kernel sysctlmibinfo #

**This project is obsolete, new interface: https://gitlab.com/alfix/sysctlinfo**

## 1 Introduction ##

The FreeBSD's kernel maintains a Management Information Base where the objects 
are properties to tuning the system using the _sysctl()_ syscall and the 
_/sbin/sysctl_ utility..

A 'property' has some info (description, type, label, etc.), 
they are necessary to build an utility like /sbin/sysctl, example:

	% sysctl -d kern.ostype
	kern.ostype: Operating system type
	% sysctl -t kern.ostype
	kern.ostype: string

The kernel provides an undocumented API to pass this info to the 
userland. The purpose of _kernel-sysctlmibinfo_ is to provide a _better API_: 
implemented by a set of sysctl nodes {0.7-8-9-10-11-12-13.} and {0.3} 
(kernel API); in the future I could replace these nodes with a syscall.

### 1.1 FAQ ###

* Why ksysctlmibinfo?   
   I wrote sysctlmibinfo to improve the performance of my utilities.
* Why not the kernel interface?   
   See "3 Off-topic" below 
* Can ksysctlmibinfo and the kernel interface coexist?   
   Yes, they can.
   sysctl mib is preserved.
* A Port/Package?   
   Maybe in the future.


--------------------------------------------------------------------------------

## 2 API ##

_kernel-sysctlmibinfo_ is closely kernel related, you could use 
[sysctlmibinfo(3)](https://gitlab.com/alfix/sysctlmibinfo) 
for a high level interface (sysctlmibinfo uses the _kernel API_, 
but it could depend on _ksysctlmibinfo_ in the future).

## 2.1 Overview ##

Pseudocode:
```c
/* from old API */
sysctl( {0.3},  2, id, &idlen, name, namelen);           // sysctl.entryname2id
/* new API */
sysctl( {0.7},  2, name, &namelen, id, idlevel);         // sysctl.entryname
sysctl( {0.8},  2, desc, &desclen, id, idlevel);         // sysctl.entrydesc
sysctl( {0.9},  2, label, &labellen, id, idlevel);       // sysctl.entrylabel
sysctl( {0.10}, 2, kind, &kindlen, id, idlevel);         // sysctl.entrykind
sysctl( {0.11}, 2, fmt, &fmtlen, id, idlevel);           // sysctl.entryfmtstr
sysctl( {0.12}, 2, nextnode, &nextnodelen, id, idlevel); // sysctl.entrynextnode
sysctl( {0.13}, 2, nextleaf, &nextleaflen, id, idlevel); // sysctl.entrynextleaf
```

### 2.2 example ###

[example.c](example.c) shows all the features.

--------------------------------------------------------------------------------

## 3 Off-topic ##

### 3.1 kernel API ###

```c
/*
 * {0,1,...}	return the name of the "..." OID.
 * {0,2,...}	return the next OID.
 * {0,3}	return the OID of the name in "new"
 * {0,4,...}	return the kind & format info for the "..." OID.
 * {0,5,...}	return the description of the "..." OID.
 * {0,6,...}	return the aggregation label of the "..." OID.
 */
```

This API has 2 "styles", pseudocode:
```c
/* -1- {0,3} to get get id-by-name */
sysctl([0,2], *id, &idlevel, name, namelen);

/* -2- {0,1-2-4-5-6} to get name, next, flags-type-fmt, desc and label by id */
memcpy([0,X] + 2, id, idlevel * sizeof(int));
sysctl([0,X,..id..], idlevel + 2, property, &propertylen, NULL, 0);
```

 * CTL\_MAXNAME is 24 so idlevel+2 could be 26 then this API can manage 
   entries up to 22 levels (a simple patch "_CTL\_MAXNAME+2_" in 
   _kern\_sysctl.c_ could fall in a "semantic error").

Consequently /sbin/sysctl fails with a state of 23 or 24 levels, 
_testing/addnodes.c_ adds 

 * a1.a2.a3.a4.a5.a6.a7.a8.a9.a10.a11.a12.a13.a14.a15.a16.a17.a18.a19.a20.a21.a22
 * b1.b2.b3.b4.b5.b6.b7.b8.b9.b10.b11.b12.b13.b14.b15.b16.b17.b18.b19.b20.b21.b22.b23
 * c1.c2.c3.c4.c5.c6.c7.c8.c9.c10.c11.c12.c13.c14.c15.c16.c17.c18.c19.c20.c21.c22.c23.c24

```
% sysctl b1
sysctl: sysctl(getnext) -1 88: Cannot allocate memory
% sysctl b1.b2.b3.b4.b5.b6.b7.b8.b9.b10.b11.b12.b13.b14.b15.b16.b17.b18.b19.b20.b21.b22.b23
sysctl: sysctl fmt -1 1024 22: Invalid argument
```

 * A struct sysctl\_oid has: oid\_kind and oid\_type, {0.4} joins them and 
   returns a void* `||3 bytes flags - 1 byte type | format_string '\0'||`; 
   useless computation in userspace to re-split.
 * the {0.1} entry returns always "0" (false positive) building a "fake" name,
   example 1.1.100.500.1000 -> "kern.ostype.100.500.1000" (testing/bad\_name.c).
 * {0.6} returns only "next-leaf", 
   [sysctlview](https://gitlab.com/alfix/sysctlview) and 
   "[nsysctl](https://alfix.gitlab.io/bsd/2019/02/19/nsysctl-tutorial.html) -I" 
   need "next-node" -> extra computation in userspace.
 * sysctl\_sysctl\_oidfmt(), sysctl\_sysctl\_oiddescr() and 
   sysctl\_sysctl\_oidlabel() differ in a few lines but can not be merged 
   because SYSCTL_NODE can not "carry" a flag to "switch".
 * inefficiency, e.g. if you want name, type and format of kern.ostype you need
   3 visits of the tree to find the same node

### 3.2 Solution to improve the kernel interface ###

This solution solves the problem of the "CTL\_MAXNAME+2 level" updating the 
existing interface in kern\_sysctl.c. 
A function can handle "2 styles" simultaneously: it is enough an 'if' 
to figure out where to get the id-idlevel of the state. An example
is _name_ in compatibility\_ksysctlmibinfo.c:

```c
static int
ksysctlmif_name(SYSCTL_HANDLER_ARGS)
{
/* ... */ 

	if(arg2 != 0) {
		/* sysctl([0,X,..id..], idlevel + 2, prop, &proplen, NULL, 0); */
		id = (int*)arg1;
		idlevel = arg2;
	} else {
		/* sysctl([0,X], 2, prop, &proplen, id, idlevel); */
		error = get_object_id_from_userland(req,id,&idlevel);
		if(error != 0)
			return error;
	}
	
/* ... */
}

   
static SYSCTL_NODE(_sysctl, ENTRYNAME, entryname, 
			CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW, 
			ksysctlmif_name, "name of a state");
			
/*
static SYSCTL_NODE(_sysctl, 1, name, CTLFLAG_RD | CTLFLAG_MPSAFE | CTLFLAG_CAPRD,
    sysctl_sysctl_name, ""); 
*/
```

Now you can get the name 

```c
    /* Old API*/
    int mib[CTL_MAXNAME];
    int error = 0;
    mib[0] = 0;
    mib[1] = 1; /* Old API*/
    memcpy(mib + 2, id, idlevel * sizeof(int));
    error = sysctl(mib, idlevel + 2, (void *)name, namelen, NULL, 0);

    /* OR */

    /* New API*/
    int _mib[2];
    int _error = 0;
    _mib[0] = 0;
    _mib[1] = ENTRYNAME;
    _error = sysctl(_mib, 2, (void *)name, namelen, id, idlevel * sizeof(int));
```
