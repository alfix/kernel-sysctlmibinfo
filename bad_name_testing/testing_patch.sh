#! /bin/sh

d=${1:-`pwd`};shift

~/kernel-sysctlmibinfo/bad_name_testing/bad_name > ${d}/bad_name.txt

~/nsysctl/nsysctl -aN > ${d}/nsysctl_aN.txt
~/nsysctl/nsysctl -aIN > ${d}/nsysctl_aIN.txt
sysctl -aN > ${d}/sysctl_aN.txt

counter=0

while [ $counter -lt 10 ]
do
	time -a -o ${d}/time_nsysctl_aN.txt ~/nsysctl/nsysctl -aN 
	time -a -o ${d}/time_nsysctl_aIN.txt ~/nsysctl/nsysctl -aIN 
	time -a -o ${d}/time_sysctl_aN.txt sysctl -aN
	counter=`expr $counter + 1`
done


