/* bad_name.c
 *
 * % cc bad_name.c -o bad_name
 * % ./bad_name
 * id: [5]: 1.1.100.500.1000
 * error: 0, buflen: 25
 * error: 0, buflen: 25, name: kern.ostype.100.500.1000
 * % 
 *
 * error = 0 is a false positive and "kern.ostype.100.500.1000" is a fake name.
 */

#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>

#define BUFSIZE    1024

int
sysctl_get_name(int *id, size_t idlevel, char *name, size_t *namelen)
{
    int mib[CTL_MAXNAME + 2];
    int error = 0;

    mib[0] = 0;
    mib[1] = 1;
    memcpy(mib + 2, id, idlevel * sizeof(int));
    error = sysctl(mib, idlevel + 2, (void *)name, namelen, NULL, 0);

    return (error);
}

int main()
{
    int id[CTL_MAXNAME], i, error;
    char buf[BUFSIZE];
    size_t idlevel, buflen;
    
    id[0]=1;
    id[1]=1;
    id[2]=100;
    id[3]=500;
    id[4]=1000;
    idlevel=5;

    printf("id: [%zu]: ", idlevel);
    for (i = 0; i < idlevel; i++)
	printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');
 
    buflen = 0;
    error = sysctl_get_name(id, idlevel, NULL ,&buflen);
    printf("error: %d, buflen: %zu\n", error, buflen);
    
    memset(buf, 0, sizeof(buf));
    error = sysctl_get_name(id, idlevel, buf, &buflen);
    printf("error: %d, buflen: %zu, name: %s\n", error, buflen, buf);

    return (0);
}


