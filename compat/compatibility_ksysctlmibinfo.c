/*-
 * SPDX-License-Identifier: BSD-2-Clause-FreeBSD
 *
 * Copyright (c) 2019 Alfonso S. Siciliano https://alfix.gitlab.io
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/sysctl.h>

#include "kern_sysctl.h"
#include "ksysctlmibinfo.h"

static struct sysctl_ctx_list clist;

/* Util */

static int
get_object_id_from_userland(struct sysctl_req *req, int* id, unsigned int *idlevel)
{
	int error=0;

	if (!req->newlen)
		return (ENOENT);
	if (req->newlen > CTL_MAXNAME * sizeof(int))
		return (ENAMETOOLONG); /* XXX FIX */
	if(req->newlen % sizeof(int) != 0)
		return 1; /* XXX FIX */
	if((error = SYSCTL_IN(req, id, req->newlen)) !=0 )
		return (error);

	*idlevel = req->newlen / sizeof(int);

	return error;
}

/* API Implementation */

static int
ksysctlmif_name(SYSCTL_HANDLER_ARGS)
{
	int error = 0, listlen = 0, i;
	struct sysctl_oid *oid;
	struct rm_priotracker tracker;
	struct sysctl_oid *objlist[CTL_MAXNAME];
	int id[CTL_MAXNAME];
	unsigned int idlevel;

	if(arg2 != 0) {
		/* sysctl([0,X,..id..], idlevel + 2, prop, &proplen, NULL, 0); */
		id = (int*)arg1;
		idlevel = arg2;
	} else {
		/* sysctl([0,X], 2, prop, &proplen, id, idlevel); */
		error = get_object_id_from_userland(req,id,&idlevel);
		if(error != 0)
			return error;
	}

	SYSCTL_RLOCK(&tracker);

	error = REUSE_sysctl_find_oid(id, idlevel, &oid, NULL, req);
	if (error)
		goto out;

	memset(objlist, 0, sizeof(objlist));
	listlen = REUSE_sysctl_search_oid(objlist, oid);
	if (listlen < 0)
		goto out;
	
	for(i=0; i< listlen; i++) {
		if (req->oldidx)
			error = SYSCTL_OUT(req, ".", 1);
		if (!error)
			error = SYSCTL_OUT(req, objlist[i]->oid_name,
				strlen(objlist[i]->oid_name));
		if (error)
			goto out;
	}

	error = SYSCTL_OUT(req, "", 1);
	
 out:
	SYSCTL_RUNLOCK(&tracker);
	return (error);
}

static int
ksysctlmif(SYSCTL_HANDLER_ARGS)
{
    	struct sysctl_oid *oid;
	struct rm_priotracker tracker;
	int error, id[CTL_MAXNAME];
	unsigned int idlevel;

	error = get_object_id_from_userland(req,id,&idlevel);
	if(error != 0)
		return error;

	SYSCTL_RLOCK(&tracker);	
	error = REUSE_sysctl_find_oid(id, idlevel, &oid, NULL, req);
	if (error)
		goto out;
		
	switch(arg2) {
	case ENTRYDESC:
		if (oid->oid_descr == NULL) {
			error = ENOENT;
			goto out;
		}
		error = SYSCTL_OUT(req, oid->oid_descr, strlen(oid->oid_descr) + 1);
		break;
	case ENTRYFMT:
		if (oid->oid_fmt == NULL) {
			error = ENOENT;
			goto out;
		}
		error = SYSCTL_OUT(req, oid->oid_fmt, strlen(oid->oid_fmt) + 1);
		break;
	case ENTRYLABEL:
		if (oid->oid_label == NULL) {
			error = ENOENT;
			goto out;
		}
		error = SYSCTL_OUT(req, oid->oid_label, strlen(oid->oid_label) + 1);
		break;
	case ENTRYKIND:
		error = SYSCTL_OUT(req, &oid->oid_kind, sizeof(oid->oid_kind));
		break;
	}
	
 out:
	SYSCTL_RUNLOCK(&tracker);
	return (error);
}

static int
ksysctlmif_next(SYSCTL_HANDLER_ARGS)
{
	struct sysctl_oid *oid;
	struct sysctl_oid_list *lsp = &sysctl__children;
	struct rm_priotracker tracker;
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME], i, idnextlevel, error;
	unsigned int idlevel;

	error = get_object_id_from_userland(req,id,&idlevel);
	if(error != 0)
		return error;
	
	SYSCTL_RLOCK(&tracker);	
	i = REUSE_sysctl_sysctl_next_ls(arg2, lsp, id, idlevel, idnext, &idnextlevel, 1, &oid);
	SYSCTL_RUNLOCK(&tracker);

	if (i)
		return (ENOENT);
	
	error = SYSCTL_OUT(req, idnext, idnextlevel * sizeof (int));
	
	return (error);
}

/* Kernel Module */

static int
ksysctlmibinfo_modevent(module_t mod __unused, int event, void *arg __unused)
{   
    int error = 0;

    switch (event) {
    case MOD_LOAD:
	sysctl_ctx_init(&clist);
	SYSCTL_INIT();
	/*
	 * XXXRW/JA: Shouldn't return name data for nodes that we don't permit in
	 * capability mode.
	 */
	static SYSCTL_NODE(_sysctl, ENTRYNAME, entryname, 
			CTLFLAG_RW | CTLFLAG_ANYBODY  |CTLFLAG_MPSAFE | CTLFLAG_CAPRW, 
			ksysctlmif_name, "name of a state");
	/*SYSCTL_PROC(_sysctl, ENTRYNAME, entryname,
			CTLTYPE_STRING | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, 0, ksysctlmif_name, "A", "name of a state");*/
	SYSCTL_PROC(_sysctl, ENTRYDESC, entrydesc,
			CTLTYPE_STRING | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYDESC, ksysctlmif, "A", "description of a state");
	SYSCTL_PROC(_sysctl, ENTRYLABEL, entrylabel,
			CTLTYPE_STRING | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYLABEL, ksysctlmif, "A", "label of a state");
	SYSCTL_PROC(_sysctl, ENTRYKIND, entrykind,
			CTLTYPE_UINT | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYKIND, ksysctlmif, "IU", "kind (flags and type) of a state");
	SYSCTL_PROC(_sysctl, ENTRYFMT, entryfmt,
			CTLTYPE_STRING | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYFMT, ksysctlmif, "A", "format string of a state");
	SYSCTL_PROC(_sysctl, ENTRYNEXTNODE, entrynextnode,
			CTLTYPE_INT | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYNEXTNODE, ksysctlmif_next, "I", "next mib entry (internal node or leaf)");
	SYSCTL_PROC(_sysctl, ENTRYNEXTLEAF, entrynextleaf,
			CTLTYPE_INT | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYNEXTLEAF, ksysctlmif_next, "I", "next mib entry (only leaf)");
	uprintf("ksysctlmibinfo module loaded.\n");
	break;
    case MOD_UNLOAD:
	if (sysctl_ctx_free(&clist)) {
	    uprintf("sysctl_ctx_free failed.\n");
	    return (ENOTEMPTY);
	}
	uprintf("ksysctlmibinfo module unloaded.\n");
	break;
    default:
	error = EOPNOTSUPP;
	break;
    }
    return (error);
}
static moduledata_t ksysctlmibinfo_mod = {
    "ksysctlmibinfo",
    ksysctlmibinfo_modevent,
    NULL
};

DECLARE_MODULE(ksysctlmibinfo, ksysctlmibinfo_mod, SI_SUB_EXEC, SI_ORDER_ANY);

