#include <sys/types.h>
#include <sys/sysctl.h>

#include <stdio.h>
#include <string.h>

#include "../ksysctlmibinfo.h"

int sysctl_old_name(int *id, size_t idlevel, char *name, size_t *namelen);
int sysctl_new_name(int *id, size_t idlevel, char *name, size_t *namelen);

#define BUFSIZE    1024

int main()
{
    int id[CTL_MAXNAME], i;
    char buf[BUFSIZE];
    size_t idlevel, buflen;

    id[0]=1;
    id[1]=1;
    idlevel=2;

    printf("id [%zu]: ",idlevel);
    for (i = 0; i < idlevel; i++) {
	printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');
    }

    printf("--- OLD ------------\n");
    buflen = BUFSIZE;
    if (sysctl_old_name(id, idlevel, NULL ,&buflen) != 0) {
	printf("Error NAMELEN()\n");
    }
    memset(buf, 0, sizeof(buf));
    printf("name [%zuB]: ", buflen);
    if (sysctl_old_name(id, idlevel, buf, &buflen) != 0) {
	printf("Error _name()\n");
    }
    printf("%s\n", buf);

    printf("--- NEW ------------\n");

    buflen = BUFSIZE;
    if (sysctl_new_name(id, idlevel, NULL ,&buflen) != 0) {
	printf("Error NAMELEN()\n");
    }
    memset(buf, 0, sizeof(buf));
    printf("name [%zuB]: ", buflen);
    if (sysctl_new_name(id, idlevel, buf, &buflen) != 0) {
	printf("Error _name()\n");
    }
    printf("%s\n", buf);
    
    
    printf("\n=== False Positive ===\n\n");
    
    id[0]=1;
    id[1]=1;
    id[2]=100;
    id[3]=500;
    id[4]=1000;
    idlevel=5;

    printf("id [%zu]: ",idlevel);
    for (i = 0; i < idlevel; i++) {
	printf("%d%c", id[i], i+1 < idlevel ? '.' : '\n');
    }

    printf("--- OLD ------------\n");
    buflen = BUFSIZE;
    if (sysctl_old_name(id, idlevel, NULL ,&buflen) != 0) {
	printf("Error NAMELEN()\n");
    }
    memset(buf, 0, sizeof(buf));
    printf("name [%zuB]: ", buflen);
    if (sysctl_old_name(id, idlevel, buf, &buflen) != 0) {
	printf("Error _name()\n");
    }
    printf("%s\n", buf);

    printf("--- NEW ------------\n");

    buflen = BUFSIZE;
    if (sysctl_new_name(id, idlevel, NULL ,&buflen) != 0) {
	printf("Error NAMELEN()\n");
    }
    memset(buf, 0, sizeof(buf));
    printf("name [%zuB]: ", buflen);
    if (sysctl_new_name(id, idlevel, buf, &buflen) != 0) {
	printf("Error _name()\n");
    }
    printf("%s\n", buf);

    printf("\n=== 24-idlevel ===\n\n");

    char *dchar = "c1.c2.c3.c4.c5.c6.c7.c8.c9.c10.c11.c12.c13.c14.c15.c16.c17.c18.c19.c20.c21.c22.c23.c24";

    /* nametoid */
    bzero(id, sizeof(id));
    idlevel = CTL_MAXNAME;
    if (sysctlnametomib(dchar, id, &idlevel) != 0) {
	    printf("error sysctlnametomib\n");
    }		

    printf("--- OLD ------------\n");
    buflen = BUFSIZE;
    if (sysctl_old_name(id, idlevel, NULL ,&buflen) != 0) {
	printf("Error NAMELEN()\n");
    }
    memset(buf, 0, sizeof(buf));
    printf("name [%zuB]: ", buflen);
    if (sysctl_old_name(id, idlevel, buf, &buflen) != 0) {
	printf("Error _name()\n");
    }
    printf("%s\n", buf);

    printf("--- NEW ------------\n");

    buflen = BUFSIZE;
    if (sysctl_new_name(id, idlevel, NULL ,&buflen) != 0) {
	printf("Error NAMELEN()\n");
    }
    memset(buf, 0, sizeof(buf));
    printf("name [%zuB]: ", buflen);
    if (sysctl_new_name(id, idlevel, buf, &buflen) != 0) {
	printf("Error _name()\n");
    }
    printf("%s\n", buf);


    return (0);
}

int
sysctl_old_name(int *id, size_t idlevel, char *name, size_t *namelen)
{
    int mib[CTL_MAXNAME + 2];
    int error = 0;

    mib[0] = CTL_SYSCTLMIB;
    mib[1] = 1; /* Old API*/
    memcpy(mib + 2, id, idlevel * sizeof(int));

    error = sysctl(mib, idlevel + 2, (void *)name, namelen, NULL, 0);

    return (error);
}

int
sysctl_new_name(int *id, size_t idlevel, char *name, size_t *namelen)
{
    int mib[2];
    int error = 0;

    mib[0] = CTL_SYSCTLMIB;
    mib[1] = ENTRYNAME;
    //memcpy(mib + 2, id, idlevel * sizeof(int));

    //error = sysctl(mib, idlevel + 2, (void *)name, namelen, NULL, 0);
    error = sysctl(mib, 2, (void *)name, namelen, id, idlevel * sizeof(int));

    return (error);
}
