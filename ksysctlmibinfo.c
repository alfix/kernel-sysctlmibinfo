/*-
 * SPDX-License-Identifier: BSD-2-Clause-FreeBSD
 *
 * Copyright (c) 2019 Alfonso S. Siciliano https://alfix.gitlab.io
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/sysctl.h>

#include "kern_sysctl.h"
#include "ksysctlmibinfo.h"

static struct sysctl_ctx_list clist;

/* Util */

static int
get_object_id_from_userland(struct sysctl_req *req, int* id, unsigned int *idlevel)
{
	int error=0;

	if (!req->newlen)
		return (ENOENT);
	if (req->newlen > CTL_MAXNAME * sizeof(int))
		return (ENAMETOOLONG); /* XXX FIX */
	if(req->newlen % sizeof(int) != 0)
		return 1; /* XXX FIX */
	if((error = SYSCTL_IN(req, id, req->newlen)) !=0 )
		return (error);

	*idlevel = req->newlen / sizeof(int);

	return error;
}

/* API Implementation */

static int
ksysctlmif(SYSCTL_HANDLER_ARGS)
{
	struct rm_priotracker tracker;
	int error = 0, id[CTL_MAXNAME], indx = 0;
	unsigned int idlevel;
	struct sysctl_oid *oid, *path[CTL_MAXNAME];
	struct sysctl_oid_list *lsp = &sysctl__children;

	error = get_object_id_from_userland(req,id,&idlevel);
	if(error != 0)
		return error;

	SYSCTL_RLOCK(&tracker);	
	
	while (indx < idlevel) {
		SLIST_FOREACH(oid, lsp, oid_link) {
			if (oid->oid_number == id[indx])
				break;
		}
		if (oid == NULL) {
			error = ENOENT;
			goto out;
		}
		path[indx] = oid;
		indx++;

		if ((oid->oid_kind & CTLTYPE) == CTLTYPE_NODE) {
			if (oid->oid_handler != NULL || indx == idlevel) {
				KASSERT((oid->oid_kind & CTLFLAG_DYING) == 0,
				    ("%s found DYING node %p", __func__, oid));
				/* a node with a handler ends the search */ 
				if(indx != idlevel) {
					error = ENOENT;
					goto out;
				}
			}
			lsp = SYSCTL_CHILDREN(oid);
		} else if (indx == idlevel) {
			if ((oid->oid_kind & CTLFLAG_DORMANT) != 0) {
				error = ENOENT;
				goto out;
			}
			KASSERT((oid->oid_kind & CTLFLAG_DYING) == 0,
			    ("%s found DYING node %p", __func__, oid));
		} else {
			error = ENOTDIR;
			goto out;
		}
	}
		
	switch(arg2) {
	case ENTRYNAME:
	    for(indx=0; indx < idlevel; indx++) {
		if (req->oldidx)
			error = SYSCTL_OUT(req, ".", 1);
		if (!error)
			error = SYSCTL_OUT(req, path[indx]->oid_name,
				strlen(path[indx]->oid_name));
		if (error)
			goto out;
	}	
	error = SYSCTL_OUT(req, "", 1);
	    break;
	case ENTRYDESC:
		if (oid->oid_descr == NULL) {
			error = ENOENT;
			goto out;
		}
		error = SYSCTL_OUT(req, oid->oid_descr, strlen(oid->oid_descr) + 1);
		break;
	case ENTRYFMT:
		if (oid->oid_fmt == NULL) {
			error = ENOENT;
			goto out;
		}
		error = SYSCTL_OUT(req, oid->oid_fmt, strlen(oid->oid_fmt) + 1);
		break;
	case ENTRYLABEL:
		if (oid->oid_label == NULL) {
			error = ENOENT;
			goto out;
		}
		error = SYSCTL_OUT(req, oid->oid_label, strlen(oid->oid_label) + 1);
		break;
	case ENTRYKIND:
		error = SYSCTL_OUT(req, &oid->oid_kind, sizeof(oid->oid_kind));
		break;
	}
	
 out:
	SYSCTL_RUNLOCK(&tracker);
	return (error);
}

static int
ksysctlmif_next(SYSCTL_HANDLER_ARGS)
{
	struct sysctl_oid *oid;
	struct sysctl_oid_list *lsp = &sysctl__children;
	struct rm_priotracker tracker;
	int id[CTL_MAXNAME], idnext[CTL_MAXNAME], i, idnextlevel, error;
	unsigned int idlevel;

	error = get_object_id_from_userland(req,id,&idlevel);
	if(error != 0)
		return error;
	
	SYSCTL_RLOCK(&tracker);	
	i = REUSE_sysctl_sysctl_next_ls(arg2, lsp, id, idlevel, idnext, &idnextlevel, 1, &oid);
	SYSCTL_RUNLOCK(&tracker);

	if (i)
		return (ENOENT);
	
	error = SYSCTL_OUT(req, idnext, idnextlevel * sizeof (int));
	
	return (error);
}

/* Kernel Module */

static int
ksysctlmibinfo_modevent(module_t mod __unused, int event, void *arg __unused)
{   
    int error = 0;

    switch (event) {
    case MOD_LOAD:
	sysctl_ctx_init(&clist);
	SYSCTL_INIT();
	/*
	 * XXXRW/JA: Shouldn't return name data for nodes that we don't permit in
	 * capability mode.
	 */
	SYSCTL_PROC(_sysctl, ENTRYNAME, entryname,
			CTLTYPE_STRING | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYNAME, ksysctlmif, "A", "name of a property");
	SYSCTL_PROC(_sysctl, ENTRYDESC, entrydesc,
			CTLTYPE_STRING | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYDESC, ksysctlmif, "A", "description of a property");
	SYSCTL_PROC(_sysctl, ENTRYLABEL, entrylabel,
			CTLTYPE_STRING | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYLABEL, ksysctlmif, "A", "label of a property");
	SYSCTL_PROC(_sysctl, ENTRYKIND, entrykind,
			CTLTYPE_UINT | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYKIND, ksysctlmif, "IU", "kind (flags and type) of a property");
	SYSCTL_PROC(_sysctl, ENTRYFMT, entryfmt,
			CTLTYPE_STRING | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYFMT, ksysctlmif, "A", "format string of a property");
	SYSCTL_PROC(_sysctl, ENTRYNEXTNODE, entrynextnode,
			CTLTYPE_INT | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYNEXTNODE, ksysctlmif_next, "I", "next mib entry (internal node or leaf)");
	SYSCTL_PROC(_sysctl, ENTRYNEXTLEAF, entrynextleaf,
			CTLTYPE_INT | CTLFLAG_RW | CTLFLAG_ANYBODY | CTLFLAG_MPSAFE | CTLFLAG_CAPRW,
			NULL, ENTRYNEXTLEAF, ksysctlmif_next, "I", "next mib entry (only leaf)");
	uprintf("ksysctlmibinfo module loaded.\n");
	break;
    case MOD_UNLOAD:
	if (sysctl_ctx_free(&clist)) {
	    uprintf("sysctl_ctx_free failed.\n");
	    return (ENOTEMPTY);
	}
	uprintf("ksysctlmibinfo module unloaded.\n");
	break;
    default:
	error = EOPNOTSUPP;
	break;
    }
    return (error);
}
static moduledata_t ksysctlmibinfo_mod = {
    "ksysctlmibinfo",
    ksysctlmibinfo_modevent,
    NULL
};

DECLARE_MODULE(ksysctlmibinfo, ksysctlmibinfo_mod, SI_SUB_EXEC, SI_ORDER_ANY);

